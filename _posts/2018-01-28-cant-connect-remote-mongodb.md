---
id: 266
title: 'Can&#8217;t Connect to Remote MongoDB'
date: '2018-01-28T14:27:27+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=266'
permalink: /cant-connect-remote-mongodb/
header:
    teaser: /assets/images/2018/01/mongodb-logo-rgb-j6w271g1xn1-972x257.jpg
categories:
    - IT
tags:
    - databases
    - mongodb
    - troubleshooting
toc: false
excerpt: "If you have a brand new install of MongoDB on a remote server, and you’re trying to connect to it with MongoDB Compass but you keep getting the following error: MongoDB not running on the provided host and port"
---
![]({{ page.header.teaser }}){: .feature-image }
Here’s a quick one for you this fine Sunday afternoon. If you have a brand new install of MongoDB on a remote server, and you’re trying to connect to it with MongoDB Compass but you keep getting the following error:

> MongoDB not running on the provided host and port

Then the issue probably has to do with IP binding in the config file,`/etc/mongod.conf`.

Around line 30, you will see the following:

`# network interfaces<br></br>net:<br></br>port: 27017<br></br>bindIp: 127.0.0.1 # Listen to local interface only, comment to listen on all interfaces.`

Despite the instructions to comment out the `bindIp` line, you must actually replace the localhost address with 0.0.0.0, instead.

Once that is done, restart (or start) MongoDB again and you should be good to go.

Hat-tip to Markus W Mahlberg in this [StackOverflow](https://stackoverflow.com/questions/30884021/mongodb-bind-ip-wont-work-unless-set-to-0-0-0-0) thread.
