---
id: 13
title: 'Read-Only Friday'
date: '2015-06-30T05:22:32+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=13'
permalink: /read-only-friday/
categories:
    - IT
tags:
    - duh
header:
    teaser: /assets/images/2015/06/read-only-friday.jpg
toc: false
---

<div>
<figure markdown=1 class="feature-image">
![Read-Only Friday O'Reilly book cover](/assets/images/2015/06/read-only-friday.jpg)
<figcaption>The law of the land</figcaption>
</figure>

</div>

Hey guys! Guess what I didn’t do last Friday!

Only read.

I don’t mean I skipped work to read a book all day, which actually sounds kind of nice. Read-only Friday is an IT tradition that decrees one should not commit changes to a production server/website/app on the last work day of the week, lest you tempt fate and have to burn precious weekend hours un-doing what you did before Monday morning.

Instead, I try to use my Fridays for minor gardening and maintenance. I’ll update administrative data on the website, such as contact information, or muck around with a new function or feature that doesn’t have a production counterpart.

As the missionary responsible for bringing the Word of Read-only Friday to the office, my sin stings that much more. However, all it really cost me was about an hour and a half of additional commute time to head back into the office and ctrl-z my shame. It certainly could have been worse.