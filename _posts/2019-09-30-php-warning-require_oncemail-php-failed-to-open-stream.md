---
id: 319
title: 'PHP Warning: require_once(Mail.php): failed to open stream'
date: '2019-09-30T21:42:47+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=319'
permalink: /php-warning-require_oncemail-php-failed-to-open-stream/
classes: wide
header:
    teaser: /assets/images/2019/09/php-logo.png
categories:
    - IT
excerpt: "This one is pretty simple but sometimes frustrating to troubleshoot because the error below has you hunting for a file called Mail.php somewhere in your document root. Spoiler alert: it’s totally not there and never will be."
toc: false
tags:
    - email
    - php
    - troubleshooting
    - webdev
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

This one is pretty simple but sometimes frustrating to troubleshoot because the error below has you hunting for a file called Mail.php somewhere in your document root. Spoiler alert: it’s totally not there and never will be.

`[Mon Sep 30 20:04:05.895978 2019] [:error] [pid 21698] [client 192.168.1.1:20918] PHP Warning: require_once(Mail.php): failed to open stream: No such file or directory in /var/www/website/public_html/sendmail.php on line 111, referer: https://www.website.com/ [Mon Sep 30 20:04:05.896121 2019] [:error] [pid 21698] [client 192.168.1.1:20918] PHP Fatal error: require_once(): Failed opening required 'Mail.php' (include_path='.:/php/includes:/usr/share/pear') in /var/www/website/public_html/sendmail.php on line 111, referer: https://www.website.com/`

Maybe this is the first PHP mail form you’ve built on this server or you’re migrating to a new one, but in any case the solution is super easy. You need to tell pear to install Mail.

`$ sudo pear install -a Mail`

Your mysterious Mail.php now lives at /usr/share/pear/Mail.php.

Hat-tip to [Koteshwar](https://stackoverflow.com/questions/27962278/php-warning-requiremail-php-failed-to-open-stream-no-such-file-or-directory), the lonely 0 reputation answer on the bottom of the page. If I had 15 reps, I’d upvote your post.