---
layout: single
title: "Struggling with Gitlab Pages and Route 53 Domain Ownership"
category:
    - IT
tags:
    - dns
    - aws
    - route53
    - gitlab
    - https
    - ssl-tls
header:
    teaser: /assets/images/2023/05/you-big-dummy.png
---
![]({{ page.header.teaser }}){: .feature-image .align-right }
If you've been to Fort Kickass before, you'll notice a different look.  I spent the last couple weeks of nights and weekends migrating off my AWS EC2 and WordPress site and onto a Gitlab Pages and Jekyll blogging platform.  The reasons for this were manyfold but what I was looking for, in a word, was _simplicity_.

## Problem

**However**, what I got into was a bit of a struggle with Amazon Route 53 and Gitlab's process to confirm ownership of the domain.  In order to make use of HTTPS, Gitlab supports autogeneration of a Lets Encrypt certificate.  Part of this process is adding records to your domain to prove you have ownership of it.  I am certainly not a DNS wizard nor do I pretend to have done anything with DNS records apart from pointing a new domain to a hosting IP address.  But domain verification is nothing I haven't done before to set up other services.

The instructions provided by Gitlab appear to tell you to create two records.  An "alias" record pointing to domain.gitlab.io and the usual `TXT` record with a verification code.  Data for both records were provided in code boxes with copy buttons.

[![Screenshot of Gitlab "Pages Domain" page instructing the user to add DNS records](/assets/images/2023/05/gitlab-pages-domain.png "Seems fairly straight forward")](/assets/images/2023/05/gitlab-pages-domain.png)

I copy and pasted the `TXT` record data into the Route 53 UI and then struggled mightily with various combinations of `A` alias records, `CNAME` records, subdomains, and alias trickery.  I broke access to this site on a few occasions with my experiments but, try as I might, I could NOT get the domain verified with Gitlab.

[![Screenshot of a Route 53 TXT record entry](/assets/images/2023/05/route53-txt-entry.png "Record created by copy/pasting Gitlab details")](/assets/images/2023/05/gitlab-pages-domain.png)

## Who's to Blame?

I directed my frustration at AWS for a long time, thinking they were artificially preventing me from inserting a record outside of the Route 53 hosted zone by only providing a dropdown menu with AWS products and records only inside the hosted zone.

[![Screenshot of a Route 53 A alias record entry with a dropdown menu of acceptable values](/assets/images/2023/05/route53-alias-entry.png "Only a dropdown menu with AWS products and no free text entry")](/assets/images/2023/05/route53-alias-entry.png)

[![Big red box with the error "...the alias target name does not lie within the target zone"](/assets/images/2023/05/error-alias.png "How dare you!")](/assets/images/2023/05/error-alias.png)

[![Big red box wiht the error "...type CNAME ... is not permitted at apex in zone"](/assets/images/2023/05/error-cname.png "I said good day!")](/assets/images/2023/05/error-cname.png)

## Solution

I decided to reach out to my [VetSec](https://veteransec.org) network for assistance and [Drew Hjelm](https://twitter.com/drewhjelm) answered the call.  After going back and forth over direct messages for a while, Drew said something that clicked in my head.  

> Drew Hjelm:  
If it’s not validating ownership then it would be the _gitlab txt record that it’s having an issue with right?

Perhaps?

I took a closer look at that `TXT` entry from the beginning of this struggle.

[![A screenshot of my TXT record verification entry in Route 53 for _gitlab-pages-verification-code-patrickriggs.com.patrickriggs.com](/assets/images/2023/05/at-last.png "Do you see it?")](/assets/images/2023/05/at-last.png)

Damnit.

I copied the data from Gitlab and just pasted it into my entries without editing for the automatic addition of `.patrickriggs.com` by AWS.  It's a record for `_gitlab-pages-verification-code-patrickriggs.com.patrickriggs.com`.

[![A small snippet of the doubled-up entry of patrickriggs.com.patrickriggs.com](/assets/images/2023/05/ya-big-dummy.png){: .align-center}](/assets/images/2023/05/ya-big-dummy.png)

It never needed an "alias" record as outlined in the Gitlab instructions.  It only ever needed the `TXT` record to confirm ownership of the site.  I was only too happy to click that little copy button on the Gitlab code block and paste it into the text box.  

Once I fixed the double entry on my `TXT` record, my domain ownership was verified about 2 minutes later.  The Let's Encrypt certificate took longer to generate than it did for the records to propagate.

And with that, I was able to close 20 Firefox tabs.
