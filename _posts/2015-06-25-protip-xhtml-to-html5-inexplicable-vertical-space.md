---
id: 9
title: 'Protip: XHTML to HTML5 Inexplicable Vertical Space'
date: '2015-06-25T14:45:31+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=9'
permalink: /protip-xhtml-to-html5-inexplicable-vertical-space/
categories:
    - IT
tags:
    - webdev
header:
    teaser: /assets/images/2015/12/html5.png
toc: false
---

<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>
If you’ve changed the DOCTYPE of an older site using XHTML to HTML5 and now your images have an inexplicable space beneath them, it’s not just you.

## Solution

`img { display: block; }`

I lost probably 6 hours trying to troubleshoot the CSS and no amount of border, padding, margin or positioning helped. My google-fu was weak early on, but I eventually stumbled on this [stackoverflow post](http://stackoverflow.com/questions/4904668/html5-vertical-spacing-issue-with-img) (see *Alohci*‘s comment for the “why”) which worked like magic.

Hope this saves you some needless grinding.