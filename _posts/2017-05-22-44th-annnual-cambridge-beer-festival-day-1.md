---
id: 162
title: 'The 44th Annual Cambridge Beer Festival, Day 1'
date: '2017-05-22T23:28:57+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=162'
permalink: /44th-annnual-cambridge-beer-festival-day-1/
geo_latitude:
    - '52.2134941'
geo_longitude:
    - '0.1150597'
geo_public:
    - '1'
header:
    teaser: /assets/images/2017/05/cambridge_beer_festival-972x393.jpg
categories:
    - 'People &amp; Culture'
tags:
    - beer
    - Cambridge
    - CAMRA
    - festival
    - 'real ale'
excerpt: "2017 marks the fourth year I have been fortunate enough to attend the Cambridge Beer Festival on Jesus Green in central Cambridge. It is my favorite annual event here and one that I will go out of my way to attend for as long as we reside in this country."
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

2017 marks the fourth year I have been [fortunate enough to attend the Cambridge Beer Festival](https://patrickriggs.com/blog/2017-cambridge-beer-festival-preview/) on Jesus Green in central Cambridge. It is my favorite annual event here and one that I will go out of my way to attend for as long as we reside in this country. I started the day by scouring the Cambridge Beer Festival app for [Murder of Crows](https://untappd.com/b/kissingate-brewery-murder-of-crows/1286909) — my personal favorite from 2016. [Kissingate Brewery](http://www.kissingate.co.uk/), however, did not see fit to bring this one so, instead, I went on the hunt for 2016’s festival winner (my #2): [Prince of Denmark](https://www.ratebeer.com/beer/harveys-prince-of-denmark/93506/) by [Harvey’s Brewery](https://www.harveys.org.uk/). Indeed, a keg of this was present, however after I worked my way into a conversation with the black-shirted brew masters milling about behind the racks of beers and ales, I learned they were letting those casks sit another day or two before opening them up. Prince of Denmark will have to wait until Wednesday, for me.

## **Beer #1**: Lavender Honey from Wolf Brewery

[Lavender Honey](https://www.beeradvocate.com/beer/profile/6906/99858/) from [Wolf brewery](http://www.wolfbrewery.com/). The beer smells exactly as it is titled. Light and crispy. 3.7% ABV. A very good, light start to the evening. Would drink this beer again or recommend it to a non-beer drinker.

## **Beer #2**: Cwtch by Tiny Rebel Brewery

…aaand into the fire. *Cwtch*, pronounced *witch* from [Tiny Rebel Brewery](http://www.tinyrebel.co.uk/). The pint has a funny nose. Strong flavor, compared to the Lavender Honey, sitting at 4.6% ABV. The odd scent could be my glass. I just washed it at the rinsing station. The flavor smoothed out as I got used to it. Very hoppy. The taste was not unpleasant, but coming on the heels of the mild offering from Wolf Brewery, it wasn’t exactly the steady increase in flavor potency I was looking for.

I was interviewed by [Cambridge 105 radio](https://cambridge105.co.uk/) for a piece on either the morning breakfast show or the afternoon drive time. Listen for the Yankee talking about the [Cambridge Beer Festival app](https://play.google.com/store/apps/details?id=ralcock.cbf&hl=en).

## **Beer #3**: DIPA by Cloudwater Brewing Company

[DIPA by Cloudwater Brew Co](http://cloudwaterbrew.co/blog/double-ipa-day). The Cloudwater stand sat in a packed corner of the tent with a disproportionately large crowd as compared to the rest of the tent. Half of their offerings were really strong, sitting nearby 10% ABV, offering only 1/3rd pints. The volunteer bartender pointed me to DIPA after I asked for their best seller. It’s a grassy, hoppy, strong pint and sits at 9.0% ABV. Lemony punch to the mouth. It’s very cloudy and looks like Apple juice. Delicious, but not what I’m looking for. Years of American beer has soured me on hops.

Going for food. Ate at the Spanish Bocata truck: Facebook, [azaharspanishfood](https://www.facebook.com/AzaharSpanishFood/); Twitter: [@azaharfoodvan](https://twitter.com/azaharfoodvan). Very, very good. Worth the wait in line.

## Beer #4: Schiehallion by Harviestoun

I linked up with a friend, James, at this point. We went on to have a half each of the 4.8% [Schiehallion by Harviestoun](https://harviestoun.com/), the makers of Old Engine Oil. At this point I’ve stopped taking tasting notes and have started concentrating on identifying the nationality of fest attendees based on their clothing. Easy mode: American.

## **Beer #5**: Gigesa Guji by Three Blind Mice

[Gigesa Guji](https://untappd.com//b/three-blind-mice-brewery-gigesa-guji/2021339#_=_) by [Three Blind Mice](https://untappd.com/ThreeBlindMiceBrewery). THIS was the winner of night one. Strong, sweet 4.6% ABV coffee stout. Perfect cap to the evening. Lavender Honey for the mild start and the strong coffee Gigesa Gugi to carry me home. The three in the middle were a little out of place for me. This is the third consecutive year I find myself not excited about hops and IPAs. I still think it was the saturation of IPAs back in the States that have since turned me off.

Find me on Untappd at [PaddyYankee](https://untappd.com/user/PaddyYankee).