---
id: 107
title: 'Pay No Attention to That Laughing Man Behind the Curtain'
date: '2017-05-02T20:53:48+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=107'
permalink: /pay-no-attention-to-that-laughing-man-behind-the-curtain/
header:
    teaser: /assets/images/2017/05/stackoverflow03-972x445.png
categories:
    - General
    - IT
tags:
    - humor
    - stackoverflow
toc: false
---

One thing I’ve always loved about the Internet are the odd bits of humanity that periodically emerge from behind the digital curtains.

Today, I was troubleshooting a rather simple JavaScript problem and stumbled my way into a Stackoverflow.com error page containing an unexpected image. After a chuckle, I clicked refresh hoping the error had been solved, not immediately realizing I had been redirected to a static error page. I was rewarded with a second image, then a third. Each simultaneously eliciting another chuckle… and questions. Who were these people? When were these pictures taken? Where did they get that laptop?

[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow01-1024x474.png)](/assets/images/2017/05/stackoverflow01.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow16-1024x468.png)](/assets/images/2017/05/stackoverflow16.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow02-1024x473.png)](/assets/images/2017/05/stackoverflow02.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow15-1024x467.png)](/assets/images/2017/05/stackoverflow15.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow05-1024x425.png)](/assets/images/2017/05/stackoverflow05.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow04-1024x473.png)](/assets/images/2017/05/stackoverflow04.png)[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow09-1024x469.png)](/assets/images/2017/05/stackoverflow09.png)[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow06-1024x471.png)](/assets/images/2017/05/stackoverflow06.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow07-1024x471.png)](/assets/images/2017/05/stackoverflow07.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow17-1024x468.png)](/assets/images/2017/05/stackoverflow17.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow08-1024x470.png)](/assets/images/2017/05/stackoverflow08.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow11-1024x469.png)](/assets/images/2017/05/stackoverflow11.png)  
[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow10-1024x471.png)](/assets/images/2017/05/stackoverflow10.png)[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow13-1024x467.png)](/assets/images/2017/05/stackoverflow13.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow14-1024x467.png)](/assets/images/2017/05/stackoverflow14.png) [![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow03-1024x469.png)](/assets/images/2017/05/stackoverflow03.png)[![Screen capture of a Stackoverflow.com error](/assets/images/2017/05/stackoverflow12-1024x509.png)](/assets/images/2017/05/stackoverflow12.png)