---
id: 36
title: 'VMWare Workstation Isn&#8217;t Working With My Keyboard'
date: '2016-08-12T16:15:17+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=36'
permalink: /vmware-workstation-isnt-working-with-my-keyboard/
categories:
    - IT
tags:
    - Synergy
    - tech
    - VMWare
toc: false
---

“Further, when it does capture my keyboard it keeps repeating the same key infinitellllllllllllllllll….”

Are you running [Synergy](http://synergy-project.org/) or any other piece of software whose core function is inserting themselves between your input device and the operating system? Turn it off. Voila.