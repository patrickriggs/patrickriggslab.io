---
id: 32
title: 'We Are All Periodically Humbled by the Basics'
date: '2016-07-06T06:22:24+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=32'
permalink: /we-are-all-periodically-humbled-by-the-basics/
header:
    teaser: /assets/images/2016/07/encryption.jpeg
categories:
    - IT
tags:
    - encryption
    - firewall
    - https
    - iptables
    - 'mistakes were made'
    - SSL
    - TLS
toc: false
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

Yesterday, I solved a problem that has been plaguing a colleague and I for three weeks.

The task was to install a server certificate, enabling HTTPS. Seems simple enough. I did it for the first time on one of my personal projects in 40 minutes over coffee one morning.

Not so simple at the office. Nothing we did worked. I had burned almost three weeks of full-time effort on this stupid thing. I blamed it on everything including the certificate issuer, the previous system administrators, our version of Apache, and our Information Assurance people — would they honestly be stupid enough to block port 443? Of course they wouldn’t.

But I would.

In my frustration-fueled haste, I had missed a stupid line from the output of `iptables -L`. Or rather, I hadn’t noticed this line was missing:

`ACCEPT     tcp  --  anywhere           anywhere           tcp dpt:<span style="color: #ff0000;">https</span>`

I knew early on that something was off because we weren’t getting any evidence that a request for a resource was being logged by Apache, which is why I had blamed the Information Assurance people for about a day. A request via `curl -O https://etc` was working, as long as we specifically gave it a list of valid certificate authorities — because it wasn’t getting them via our Apache configuration, for some reason (hint!).

But that result only served to obfuscate the problem. It was working because it was a command executed on the same server. Nothing ever worked via the browser.

It’s all about the basics and I was wrecked and wracked with misgivings about my professional abilities. I couldn’t even revel in my victory last night. It felt hollow.

This morning is better and today will be better.