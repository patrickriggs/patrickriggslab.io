---
id: 506
title: 'AutoRecon Broke!'
date: '2022-04-30T13:07:45+02:00'
author: Patrick
layout: single
guid: 'https://patrickriggs.com/blog/?p=506'
permalink: /autorecon-broke/
image: /wp-content/uploads/2022/04/AutoReconSnip.png
categories:
    - Hacking
    - IT
tags:
    - hacking
    - 'information security'
    - security
excerpt: "“I used [Tib3rius’s AutoRecon](https://github.com/Tib3rius/AutoRecon.git) successfully, like, a week ago but now it’s broken!”  Chances are that you also recently ran an `apt update && apt upgrade` during the intervening period and now your trusty `sudo $(which autorecon)` commands are complaining about not knowing what autorecon is."
header:
    teaser: /assets/images/2022/04/AutoReconSnip.png
---

## Problem

“I used [Tib3rius’s AutoRecon](https://github.com/Tib3rius/AutoRecon.git) successfully, like, a week ago but now it’s broken!”

Chances are that you also recently ran an `apt update && apt upgrade` during the intervening period and now your trusty `sudo $(which autorecon)` commands are complaining about not knowing what autorecon is.

See “Related Errors” below to determine if my problem’s fingerprint matches yours.

## Solution

Delete the pipx modules directory, `~/.local/pipx`, and pipx re-install autorecon as outlined in the [GitHub documentation](https://github.com/Tib3rius/AutoRecon#installation-method-1-pipx-recommended).

```
┌──(2022.04.30 | 13:09:40)──[~/.local]
└─$ rm -rf pipx

┌──(2022.04.30 | 13:10:56)──[~/.local]
└─$ pipx install git+https://github.com/Tib3rius/AutoRecon.git
<strong>  installed package autorecon 2.0.21, installed using Python 3.10.4</strong>
  These apps are now globally available
    - autorecon
done! ✨ 🌟 ✨

┌──(2022.04.30 | 13:12:30)──[~/hacking/thm/holo]
└─$ sudo $(which autorecon) --dirbuster.tool feroxbuster -t ./ips.txt
[!] It looks like the config/plugins in /root/.config/AutoRecon are outdated. Please remove the /root/.config/AutoRecon directory and re-run AutoRecon to rebuild them.
[*] Scanning target 10.200.x.x
[*] Scanning target 10.200.x.x
...
```

## Explanation

At some point, your python3 was upgraded to a new version and broke the module’s dependency to the older version your pipx modules were using. The quickest solution to get back to your hacking is to just delete `~/.local/pipx` and re-install the autorecon module.

## Solution Source

[GitHub pipx Issue 278](https://github.com/pypa/pipx/issues/278)

## Original Errors

```
┌──(2022.04.30 | 12:41:53)──[~/hacking/thm/holo]
└─$ sudo $(which autorecon) --dirbuster.tool feroxbuster -t ./ips.txt 
Traceback (most recent call last):
File "/home/tac0shell/.local/bin/autorecon", line 5, in
from autorecon.main import main
<strong>ModuleNotFoundError: No module named 'autorecon'</strong>

┌──(2022.04.30 | 13:05:10)──[~/.local/pipx/venvs/autorecon]
└─$ pipx list                                                 
venvs are in /home/tac0shell/.local/pipx/venvs
apps are exposed on your $PATH at /home/tac0shell/.local/bin
<strong>   package autorecon 2.0.16, installed using Python 3.9.10</strong>
    - autorecon
   package pyinstaller 3.6, installed using Python 3.9.12
    - pyi-archive_viewer
    - pyi-bindepend
    - pyi-grab_version
    - pyi-makespec
    - pyi-set_version
    - pyinstaller

┌──(2022.04.30 | 13:05:33)──[~/.local/pipx/venvs/autorecon]
└─$ pipx upgrade autorecon
/home/tac0shell/.local/pipx/shared/bin/python: No module named pip
Failed to upgrade shared libraries
Traceback (most recent call last):
  File "/home/tac0shell/.local/lib/python3.10/site-packages/pipx/shared_libs.py", line 113, in upgrade
    subprocess_post_check(upgrade_process)
  File "/home/tac0shell/.local/lib/python3.10/site-packages/pipx/util.py", line 202, in subprocess_post_check
    raise PipxError(
pipx.util.PipxError: '/home/tac0shell/.local/pipx/shared/bin/python -m pip --disable-pip-version-check install -q --upgrade pip setuptools wheel' failed
/home/tac0shell/.local/pipx/venvs/autorecon/bin/python: No module named pip
'/home/tac0shell/.local/pipx/venvs/autorecon/bin/python -m pip install --upgrade git+https://github.com/Tib3rius/AutoRecon.git -q' failed

┌──(2022.04.30 | 13:10:17)──[~/.local]
└─$ python3 --version
<strong>Python 3.10.4</strong>
```