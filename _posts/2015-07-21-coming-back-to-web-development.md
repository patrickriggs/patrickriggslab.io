---
id: 17
title: 'Coming Back to Web Development'
date: '2015-07-21T10:09:34+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=17'
permalink: /coming-back-to-web-development/
categories:
    - IT
tags:
    - development
    - education
    - hiatus
    - prodev
    - 'professional development'
header:
    teaser: /assets/images/2015/07/html-coding.jpg
toc: false
---

<div class="feature-image" markdown=1>
![]({{ page.header.teaser }})
<figcaption>Back to basics</figcaption>

</div>From the years of 2007 to 2014, I was not doing web development. I went off on a related tangent that proved to be both, in turn, immensely rewarding and crushingly depressing. Regardless, the fact of the matter was that in 2007 I was approaching 30 and I was burned out on code. The passion was gone and I wanted out.

7 years later, my hiatus proved a positive as I triumphantly returned in 2014 an older, wiser developer with renewed interest and passion in the topic. I had gained many valuable secondary skills including project management, executive communication, physical fitness and two elusive degrees: an AA and a BS.

The downside is a that lot happens in the IT world over 7 years. There’s an HTML5 now? What’s jQuery? MongoNodeAngu-what?

Thankfully, the project for which I am now hired is slightly older back-end tech and heavy on JavaScript front-end (a single page app), which has given me time to slowly ramp up to the modern era. Industry magazines like [Web Designer](http://www.webdesignermag.co.uk/), [Net Magazine](http://www.creativebloq.com/net-magazine), [Linux User &amp; Developer](http://www.linuxuser.co.uk/), and a generous training budget from my employer (around 2 classes a year) have given me the tools I need to work side projects on my own and bring those skills to my day job.

Simultaneously, I returned to an old project. The same one I used to first teach myself PHP back in 2002. With it, I reacquainted/taught myself HTML5, CSS3, PHP, JS and jQuery, while applying it to the new-to-me Bootstrap framework.

I’m back and I love it.