---
id: 186
title: 'The 44th Annual Cambridge Beer Festival, Day 5'
date: '2017-05-27T08:12:09+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=186'
permalink: /cambridge-beer-festival-day-5/
categories:
    - 'People &amp; Culture'
tags:
    - beer
    - Cambridge
    - CAMRA
    - festival
    - 'real ale'
---

<span class="feature-image">
[![Cambridge Beer Festival: My votes for Beer of the Festival](/assets/images/2017/05/20170526_2028321096860315-225x300.jpg)](/assets/images/2017/05/20170526_2028321096860315.jpg "Votes cast for Baltic Trader, Prince of Denmark, Gigesa Guji, and Lavender Honey")
</span>
<figcaption>Votes cast for Baltic Trader, Prince of Denmark, Gigesa Guji, and Lavender Honey</figcaption>

Friday, day 5 of the 44th Annual Cambridge Beer Festival. This is typically my last day of exploration. The really good ones are increasingly running out by now and the increased crowds don’t permit you to have an interaction with the volunteer bartenders, seeking their recommendations as they try to grind through the requests.

## Real Mild Ale by Goacher’s Ales

\[ [Brewer](http://www.goachers.com/beers-and-ales), [Untapped](https://untappd.com/b/goacher-s-ales-real-mild-ale/136729) \]

I went hunting for “Orange Wheat” by Green Jack Brewing Company. Despite the Cambridge Beer Festival app saying there was still “some remaining,” it was all gone. So I made a knee jerk decision to have a Goacher’s Real Mild Ale and was pleasantly rewarded. Full bodied despite its mild flavor. An unassuming chocolate and malty ale you might not specifically seek out, but you wouldn’t mind having several if proffered.

## Baltic Trader by Green Jack

\[ [Brewer](http://www.green-jack.com/tastingnotes.html), [Untapped](https://untappd.com/b/green-jack-brewing-company-baltic-trader/52728) \]

Whoa! This is really good! I ordered and paid for a third… I got a delicious half a pint of 10.5% liquid whoop ass. Sweet, full bodied, coffee, vanilla, without a trace of the buzzsaw edge I get that usually steer me away from other Russian imperial stouts. The bartender tipped me off to a 3 year version of this at the KeyKeg bar. I made a straight shot there, but by the time I got to the front of the line, the bartender said they had already sold out. Damn! This is my leading candidate for personal favorite of the festival.

## The Stout Porter by Anspach &amp; Hobday

\[ [Brewer](http://www.anspachandhobday.com/products/), [Untappd](https://untappd.com/b/anspach-and-hobday-the-stout-porter/856015) \]

Having this one on the heels of the Baltic Trader is simply unfair. The Stout Porter tasted like fruit, chocolate, and coffee and I’m sure I would have immediately described it as full bodied had I just not had the Baltic Trader. It tasted just fine and probably would have been much more so, had it been allowed to stand on its own.

## Chocolate Orange Stout by Moonshine Brewery

\[ [Brewer](http://www.moonshinebrewery.co.uk/beers.html), [Untapped](https://untappd.com/b/cambridge-moonshine-chocolate-orange-stout/40298) \]

Whatever orange flavor exists is beyond my diminished capacity taste buds. It must be hidden behind the chocolate.

At the end of the night, I cast my votes for beers of the festival. I immediately assigned 5 points to Baltic Trader, Prince of Denmark, and Gigesa Guji, and then gave 4 to Lavender Honey, so as not to leave out the lighter options from my recognition. On day 6, my whole family would return to the festival and meet up with friends of ours to have an afternoon of it. I did not anticipate coming across any new dark horse candidates and wanted to ensure I had done my civic duty and just have a nice relaxing afternoon.