---
id: 340
title: 'Steam Crashes When Launching in Linux Mint'
date: '2020-03-25T13:08:27+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=340'
permalink: /steam-crashes-when-launching-in-linux-mint/
header:
    teaser: /assets/images/2020/03/steam_logo.jpg
excerpt: "I recently tried to launch Steam on my Linux Mint daily driver. A window popped up to check for updates and seemed to progress normally until it crashed. There was no feedback received, save from unhelpful errors inside /var/log/syslog."
categories:
    - IT
tags:
    - 32-bit
    - gaming
    - linux
    - Mint
    - Steam
    - GPU
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

## Problem

I recently tried to launch Steam on my Linux Mint daily driver to experience a Dungeons &amp; Dragons throwback with *Baldur’s Gate*. A window popped up to check for updates and seemed to progress normally until it crashed. There was no feedback received, save from unhelpful errors inside /var/log/syslog.

## Solution

`$ sudo apt-get install libnvidia-gl-390:i386`

## Details

My workstation is running Linux Mint 19.2 with Cinnamon 4.2.4 on a quad-core Intel i5-4590 with 8GB of RAM. The 6TB hard drive is no where near full and it runs a slightly older (2014) NVIDIA GeForce GTX 970.

The syslog entry isn’t important, as it merely told me that Steam was crashing but not precisely why. It was just useful enough, however, to get me to [this post on the Linux Mint forums](https://forums.linuxmint.com/viewtopic.php?p=1489108#p1489108).

Here is the magic command from forum user, *tuxinvader,* that made the whole thing work for me:

`$ sudo apt-get install libnvidia-gl-390:i386<br></br>`

The brief explanation is that the Steam client is a 32-bit product and my Intel i5-4590 runs a 64-bit architecture. The *libnvidia-gl-360:i386* file contains the 32-bit libraries which translate my processor’s 64-bit instructions down to 32 for Steam to understand.

Similar to someone translating the Queen’s English to someone who only speaks Cockney?

## More Detail

[Linux Mint is a Ubuntu-based Linux distribution](https://linuxconfig.org/linux-mint-vs-ubuntu). As such, I do have vague recollections of Valve (Steam) and Canonical (Ubuntu) going head-to-head regarding support for 32-bit software. I won’t [rehash that conversation](https://arstechnica.com/gaming/2019/06/steam-and-ubuntu-clash-over-32-bit-libs/), but the crux of the reason for this lack of default support is described by ArsTechnica.

> The heart of the issue is that 32-bit computing represents an incredibly wide attack surface, with lessening amounts of active maintenance to discover, analyze, and patch flaws and exploits. Canonical, like any company, needs to apply its developer resources intelligently, so it looks for ways to remove unnecessary cruft where possible. The vast majority of 32-bit code is cruft.

And further, from [Canonical](https://ubuntu.com/blog/statement-on-32-bit-i386-packages-for-ubuntu-19-10-and-20-04-lts):

> “We will put in place a community process to determine which 32-bit packages are needed to support legacy software, and can add to that list post-release if we miss something that is needed…

After installing those libraries, Steam promptly started without a hitch. I’ve already had four members of my party cut-down just outside of Beregost.