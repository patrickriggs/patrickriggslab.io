---
id: 239
title: 'Hacking Windows 7 Professional Edition, SP2'
date: '2017-11-15T08:15:21+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=239'
permalink: /hacking-windows-7-professional-edition/
header:
    teaser: /assets/images/2017/11/5601-e1510733394885.png
categories:
    - IT
tags:
    - certification
    - GPEN
    - hacking
    - 'penetration testing'
    - pentest
    - SEC560
excerpt: "Since taking the SANS SEC560 Network Penetration Testing and Ethical Hacking course earlier this summer, I’ve spent most of my time studying for the GIAC Penetration Tester (GPEN) exam, rather than practicing the skills I learned in the course."
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

Since taking the SANS SEC560 Network Penetration Testing and Ethical Hacking course earlier this summer, I’ve spent most of my time studying for the GIAC Penetration Tester (GPEN) exam, rather than practicing the skills I learned in the course. Book learning versus practical skills. After taking my second practice exam and earning a 7-point improvement from the first (passing scores, the both of them), I’m spending the last week before my exam transitioning to skills application. My hope is that I am able to morph rote memorization into practical understanding.

## Overview

I’m going to hit the high notes of hacking Windows 7. This won’t exactly be a detailed walkthrough. At the bottom are links to the web pages that informed my actions and should be enough for anyone else to duplicate my steps. In the end, it was a rather simple hack and should be a good first one right out of the gates.

## Access

My setup is a dual-boot Macbook Pro with OS X and Kali. Launching into Kali, I installed Windows 7 Professional edition, service pack 2 onto a VirtualBox virtual machine and configured a bridged internet connection (a host-only would have worked, too). Using metasploit, I activated `exploit/windows/fileformat/<a href="https://null-byte.wonderhowto.com/how-to/hack-like-pro-hack-windows-vista-7-8-with-new-media-center-exploit-0164729/">ms15_100_mcl_exe</a>`, loaded it with a `windows/meterpreter/<a href="https://www.youtube.com/watch?v=p8wrBTSdlQw">reverse_tcp</a>` payload, then created a `foo_fighters.mcl` file for my unsuspecting Windows user, *Podrick*, to download and load into Windows Media Server.

[![Hacking Windows 7: Meterpreter hashdump](/assets/images/2017/11/meterpreter_hashdump-300x165.png)](/assets/images/2017/11/meterpreter_hashdump.png)
<figcaption>Meterpreter Hashdump</figcaption>

## Privilege Escalation

Once Podrick had tried to run the file (dummy), my reverse tcp connection was established. Now I wanted privileged user access. Initial access via Podrick was in an unprivileged group. Privilege escalation would be the metasploit equivalent of Right-Click &gt; Run as Administrator. Metasploit’s `getsystem` doesn’t work on Windows 7, so I used `exploit/windows/local/<a href="https://sathisharthars.com/2014/06/19/bypass-uac-and-get-admin-privilege-in-windows-7-using-metasploit/">bypassuac</a>`, which worked quickly against my unhardened target to get me SYSTEM privileges.

## Pillage

My next goal was to obtain the password hashes to practice password cracking. From meterpreter, I ran `run post/windows/gather/<a href="https://www.offensive-security.com/metasploit-unleashed/meterpreter-basics/#hashdump">hashdump</a>`. That easily gave me the contents of the SAM database.

## Password Cracking

[![Hacking Windows 7: Ophcrack cracking Podrick's password](/assets/images/2017/11/ophcrack_001-300x189.png)](/assets/images/2017/11/ophcrack_001.png)
<figcaption>Ophcrack crack</figcaption>

As the designated password cracker on my SEC560 capture-the-flag team, this was admittedly easier during the course than at home. Presumably the entire course provided us with every tool we needed with little to no complications. Rainbow tables included.

I had to do some footwork to obtain my own rainbow tables for Ophcrack. I went with two tables available from the [Ophcrack Sourceforge](http://ophcrack.sourceforge.net/tables.php) page: *XP free small* and *Vista free*. The Vista free table cracked Podrick’s password in 3 minutes and 27 seconds*: password1234*.

## Conclusion

My first unguided hack was a great success. It was my first since taking the SANS560 course and a boost to my confidence in my own abilities. While it may not have seemed like a long hack, it took me around four sessions over a holiday weekend to sift through the links below and build my path to the cracked password.

I have already installed the [BSides London 2017](https://www.vulnhub.com/entry/hackerhouse-bsides-london-2017,202/) ISO and am staring at a picture of a raft on a river served up by Apache 2.4.10. I hope I didn’t grab too hard a target for my second attempt.

## Resources Used

[MS15-100](https://null-byte.wonderhowto.com/how-to/hack-like-pro-hack-windows-vista-7-8-with-new-media-center-exploit-0164729/)  
[reverse\_tcp](https://www.youtube.com/watch?v=p8wrBTSdlQw)  
[bypassuac/](https://sathisharthars.com/2014/06/19/bypass-uac-and-get-admin-privilege-in-windows-7-using-metasploit/)  
[hashdump](https://www.offensive-security.com/metasploit-unleashed/meterpreter-basics/#hashdump)  
[Ophcrack Rainbow Tables](http://ophcrack.sourceforge.net/tables.php)