---
id: 200
title: 'My &#8220;Leaderboard Syndrome&#8221; Moment'
date: '2017-07-10T23:21:34+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=200'
permalink: /leaderboard-syndrome-moment/
header:
    teaser: /assets/images/2017/07/leaderboards.jpg
categories:
    - IT
tags:
    - hacking
    - 'leaderboard syndrome'
    - security
excerpt: "I zoomed straight in on #1 and found an episode of Shop Talk from three years ago, [Special: One on One with a Hacker](http://shoptalkshow.com/episodes/special-one-one-hacker/). About halfway through the episode, I experienced a moment that activated something within me that I am calling, **Leaderboard Syndrome**."
toc: false
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

In the almost 2 hours I spend on the road every day, I consume a lot of podcasts. The [Shop Talk Show ](http://shoptalkshow.com/)is one I occasionally dip in to if a topic they cover piques my interest. Last week on Reddit, I found myself in a Google spreadsheet that lists the [most-shared programming podcast episodes](https://docs.google.com/spreadsheets/d/1gIRjeB1Y_AMvtmJsZWl_dNMDJ7lPSIxiVUYyEvrP5P4/htmlview?usp=sharing&sle=true). I zoomed straight in on #1 and found an episode of Shop Talk from three years ago, [Special: One on One with a Hacker](http://shoptalkshow.com/episodes/special-one-one-hacker/). About halfway through the episode, I experienced a moment that activated something within me that I am calling, **Leaderboard Syndrome**.

Over the last 3 years, this episode has received lots of deserved discussion online. It’s a really fascinating show where the podcast host and proprietor of [CSS-Tricks](https://css-tricks.com/), Chris Coyier, interviews the man who hacked him, Earl Drudge (pseudonym). It’s a frank and honest discussion where, as victim and perpetrator, the two each lay out their perspective of the hack, obtaining a form of resolution by meeting in the middle.

As a longtime developer and aspiring [white hat](https://en.wikipedia.org/wiki/White_hat_(computer_security)), a few new bits of knowledge that came from Drudge’s tale, but nothing groundbreaking. What did surprise me, however, were the gaps in knowledge Mr. Coyier’s displayed on learning about the hacking side of the equation:

> “…you posted a whole bunch of information to something called Doxbin, which I’ve never heard of”
> 
> “Apparently \[TOR\] means the onion router or something. I don’t know anything about it…”

[![TOR Logo](/assets/images/2017/07/TOR-logo.png "TOR Logo")](/assets/images/2017/07/TOR-logo.png)
<figcaption>The Onion Router aka TOR</figcaption>

To me, these were stunning statements and is where my leaderboard syndrome kicked in. It highlighted the distance between how knowledgeable I consider the founder of CSS-Tricks and host of Shop Talk to be, and his familiarity with a topic in which I feel I am a perpetual beginner. The surprising knowledge that I now hold a position on the leaderboard of a topic, hacking and network security, over someone I assumed knew everything I knew.

And Leaderboard syndrome is just that, right there. Assuming you’re not worthy of being ranked on a thing until you suddenly find ***you are***. Even if the list is the “top 10,000” instead of the “top 10,” you’re shocked to realize you’re number 9,999.

To be clear, I do not mean to imply Chris Coyier is lacking or deficient in any way. He has more than demonstrated mastery of his discipline and given much to the world of web development. I regularly make use of CSS-Tricks and often wish I had dedicated enough effort mastering a topic enough to launch an entire site. If anything, my leaderboard syndrome couldn’t exist without the stature at which I place Chris’s ability.

Currently lacking any avenue to demonstrate my skills, I consider my leaderboard syndrome to be an achievement. It means I’ve done enough grinding to at least merit level 2 — or position 9,999 of the top 10,000.