---
id: 447
title: 'A Belated Review of SEC573: Automating Information Security With Python'
date: '2021-03-10T10:21:05+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=447'
permalink: /sec573-automating-information-security-with-python/
header:
    teaser: /assets/images/2021/03/none_shall_pass.jpg
categories:
    - IT
tags:
    - certification
    - GIAC
    - GPYC
    - prodev
    - 'professional development'
    - python
    - SANS
    - SEC573
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>
Over the 20 or so years I’ve been writing code I’d never taken a class in a language. As a web developer (PHP), software engineer (Perl), or SysAdmin (bash, Python), I’d simply Googled my way to competency.

My personal journey into IT was through the U.S. Army when the Army was still figuring out how to pipeline tech through their schoolhouses (1997-98). Absent from my learning were similar baseline experiences it feels like all the CS graduates I meet can claim. No algorithm theory or crucible of mundane, academic puzzle solving here.

## Don’t Be So Cheap

And so it was that I could never bring myself to spend money on learning something as easy as Python. Why would I do that when all I needed was the right project to apply my learning to? Well, the right project never came.

But COVID did along with teleworking, a restriction on travel, conferences, and TDYs. As such, the budget at work found itself with an overrun of funds that needed spending. SANS courses for all!

With my personal slice of the windfall, I enrolled in a [SEC573 Live Online session](https://www.sans.org/cyber-security-courses/automating-information-security-with-python/) with the Crown Prince of Pythonia, [Mark Baggett](https://twitter.com/MarkBaggett). Recently, I had transferred to the Penetration Testing team and realized my basic knowledge of Python would need some supercharging.

## Tempest in a Tea Pot

I shouldn’t have waited so long nor fretted about a Python class being too basic. In fact, if I ever make my way through <SEC660: Advanced Penetration Testing, Exploit Writing, and Ethical Hacking>, I think it will be made easier by my success in SEC573.

The total capacity of my Python knowledge was over by lunch on day one. By day three I was reassembling TCP packets and on day five I was connecting to network sockets. I had done none of those things with Perl in my previous life.

My small Live Online class consisted of one beginner, two programming veterans (myself being one), and one unstoppable rock star. On the day of our capture the flag event, Mark Baggett averaged our scores on the pyWars challenges, declaring it our scoring threshold to obtain the SEC573 Challenge Coin during the capture the flag event.

## Veteran of the pyWars

Going back to my earlier statement about shared experiences and academic puzzle solving: [pyWars was my crucible](https://github.com/MarkBaggett/pyWars). Beneath the five days of lecture loomed 95 hands-on Python challenges crafted by Prince Baggett and his Pythonistas. These were mostly optional to the course but served two main purposes:

1. As an alternative activity for veteran programmers to focus on, rather than losing interest in the more basic material
2. Practical, hands-on demonstrations of lecture material

The battlefield for pyWars was almost entirely on the Python command line, occasionally stepping out to interact with files or services you would be effecting with your Python armaments.

<figure class="wp-block-image size-large is-style-default" markdown="1">
![](/assets/images/2021/03/image-1024x182.png)
<figcaption>Scoreboard on day 5</figcaption></figure>

The “capture the flag” event on day six also used the pyWars platform. Since there were no other teams to challenge, the scoring marker calculated earlier for the challenge coin was our only goal. The questions were divided among the four of us and we set to work. At its conclusion, our team had answered all 25 questions, blowing right past the threshold needed for our challenge coins.

<figure class="wp-block-image size-large is-style-default" markdown="1">
![](/assets/images/2021/03/SEC573_challenge_coin.png)
<figcaption>To the victors go the spoils</figcaption></figure>

## In sum()

The course was worth every penny and the commitment Mark Baggett brought at 3am for six straight days to a small handful of Europe-based students did not go unnoticed. Do not be afraid of enrolling in a course for a language purported to be extraordinarily beginner friendly. There are many stacks, layers, and arenas in which Python can flex its considerable muscle and there is no way your Python worldview won’t be expanded.