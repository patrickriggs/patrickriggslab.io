---
id: 160
title: '2017 Cambridge Beer Festival: Preview'
date: '2017-05-22T16:09:25+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=160'
permalink: /2017-cambridge-beer-festival-preview/
categories:
    - 'People &amp; Culture'
tags:
    - beer
    - Cambridge
    - CAMRA
    - festival
    - 'real ale'
toc: false
---

This week marks my attendance to 4 consecutive Cambridge Beer Festivals on Jesus Green. Last year, 2016, was my high point as I was able to not only attend every single day of the week long event, but my top 3 picks of beers were all in the overall top 5 winners, providing to me some level of tasting validation.

This year, I know I will miss at least one day of the festival but this is offset by the welcome attendance of my in-laws, notably my beer enthusiast father-in-law, who has been the WhatsApp recipient of many envy-inducing photos of beer over the years.