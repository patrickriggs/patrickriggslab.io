---
id: 70
title: Overwatch
date: '2017-04-29T14:32:26+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=70'
permalink: /overwatch/
header:
    teaser: /assets/images/2017/04/overwatch-972x561.png
categories:
    - General
tags:
    - gaming
    - overwatch
toc: false
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

Come find me on Overwatch. I’m usually on the Europe region as DireSquire#11479.

**Update May 1, 2023**: Still playing.