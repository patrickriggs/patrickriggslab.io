---
id: 324
title: 'Trick certbot into auto-configuring Amazon Linux 2'
date: '2019-10-03T21:08:40+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=324'
permalink: /trick-certbot-into-auto-configuring-amazon-linux-2-ami/
header:
    teaser: /assets/images/2019/10/aws-certbot.png
categories:
    - IT
excerpt: "I use Let’s Encrypt to enable SSL/TLS encryption on all my websites. Unfortunately, certbot is not as familiar with the distant relative of the Red Hat family that is Amazon Linux 2."
tags:
    - Amazon
    - AWS
    - certbot
    - https
    - webdev
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

I use Let’s Encrypt to enable SSL/TLS encryption on all my websites. It’s brain-dead simple to configure with the EFF’s companion certbot tool and has gone a long way into the massive increase in HTTPS-by-default across the web. Unfortunately, certbot is not as familiar with the distant relative of the Red Hat family that is Amazon Linux 2.

<figure class="wp-block-image" markdown=1>
![Terminal window demonstrating certbot not knowing how to "bootstrap" on Amazon Linux](/assets/images/2019/10/bad-certbot.png "Terminal window demonstrating certbot not knowing how to 'bootstrap' on Amazon Linux")
<figcaption>C’mon, certbot!</figcaption></figure>

The server on which I am configuring HTTPS encryption is a new deployment so it’s pretty vanilla. My order of operations is as follows:

1\) Download and put in place the certbot-auto script from the Electronic Frontier Foundation (EFF):

```
$ wget https://dl.eff.org/certbot-auto
$ sudo mv certbot-auto /usr/local/bin/certbot-auto
$ sudo chown root /usr/local/bin/certbot-auto
$ sudo chmod 0755 /usr/local/bin/certbot-auto
```

2\) Enable the EPEL yum repo:

```
$ sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
$ sudo yum install -y epel-release
$ sudo yum-config-manager --enable epel
$ sudo vi /etc/yum.repos.d/epel.repo

Change both "enabled=0" to "enabled=1"
```

3\) And now, the pièce de résistance, tricking certbot into acting just like you’re a Red Hat server:

```
$ sudo touch /etc/redhat-release
$ sudo /usr/local/bin/certbot-auto --apache
```

<figure class="wp-block-image" markdown="1">
![Command line screenshot of me tricking certbot into believing that I am a Red Hat box, not Amazon Linux.](/assets/images/2019/10/certbot-auto_tricked-1.png)
</figure>

Boom. Empty /etc/redhat-release file.

If you want to DIY it or go a little off-script, these are the links I used to synthesize this information:

1. Steps 1 and 2. Trouble ahead at 3: <https://certbot.eff.org/lets-encrypt/pip-apache>
2. Post from Brad “bmw,” Warren, an EFF certbot engineer: [https://community.letsencrypt.org/t/help-with-certbot-on-the-new-amazon-linux-2/49399/3](https://community.letsencrypt.org/t/help-with-certbot-on-the-new-amazon-linux-2/49399/5)
3. Adding the EPEL repo: <https://aws.amazon.com/premiumsupport/knowledge-center/ec2-enable-epel/>

> “To make certbot-auto try the Amazon Linux bootstrapping, you can add “Amazon Linux” to `/etc/issue` or create the file `/etc/redhat-release`. Red Hat and Amazon Linux bootstrapping are identical.”
> 
> <cite>[Brad Warren](https://github.com/bmw), EFF Certbot Engineer</cite>

Pretty simple trick.