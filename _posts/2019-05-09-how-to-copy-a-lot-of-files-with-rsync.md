---
id: 286
title: 'How to Copy a Lot of Files With rsync'
date: '2019-05-09T20:46:47+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=286'
permalink: /how-to-copy-a-lot-of-files-with-rsync/
header:
    teaser: /assets/images/2019/05/tux_pixel-972x972.png
categories:
    - IT
excerpt: "In order to add a new drive to my Linux desktop for a new /home partition, I needed to copy a lot of files to an external USB drive. The directory was large and had over a decade’s worth of content. cp -rp wasn’t going to work."
toc: false
classes: wide
tags:
    - linux
    - rsync
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

In order to add a new drive to my Linux desktop for a new /home partition, I needed to copy a lot of files to an external USB drive. The directory was large and had over a decade’s worth of content. cp -rp wasn’t going to work. I wanted speed, to see progress as it copied, and I didn’t want it to touch my Dropbox folder. Enter: rsync.

`sudo rsync -avc --exclude 'Dropbox' ~/* /mnt/media/bighugedrive/home_backup/`

-a is for archive mode, which is shorthand for -rlptgoD (recursive, preserve symlinks, keep permissions, keep modification timestamps, preserve group and owner ownership, preserve device and special files)

-v is for verbose mode. Show me the files.

-c is for checksum mode. This flag skips files that have identical checksums, rather than file size and modification time. A more accurate way of not repeating yourself.

–exclude is the option that tells rsync what NOT to copy, relative to the source directory, in this case ~/.

I fumbled initially with the –exclude option by feeding it ~/Dropbox and /home/user/Dropbox. It wasn’t until [this page made it clear the value was meant to be relative](https://linuxize.com/post/how-to-exclude-files-and-directories-with-rsync/) that I was able to avoid that whole knot that is my Dropbox folder.