---
layout: single
title: "Kali: Fresh Install Checklist"
category:
    - IT
    - Hacking
tags:
    - kali
    - linux
    - hacking
    - infrastructure
header:
    teaser: /assets/images/2023/11/kali-tm.png
date: 2023-11-30 16:49:26 +0000
last_modified_at: 2024-09-05 18:15:00 +0000
excerpt: "While Kali Linux is ready to use out-of-the-box, the longer you live inside it the more opinions you realize you have about your surroundings, as the friction-reducing tweaks you make begin to stack up.  Ultimately, you find yourself performing the same post-install actions over and over and you find yourself wishing \"if only I had written this down,\" so I did."
---
While Kali Linux is ready to use out-of-the-box, the longer you live inside it the more opinions you realize you have about your surroundings, as the friction-reducing tweaks you make begin to stack up.  Ultimately, you find yourself performing the same post-install actions over and over and you find yourself wishing "if only I had written this down," so I did.

The following article contains the steps I take after installing or opening up a fresh Kali Linux VM.  These instructions are absolutely not required to use Kali Linux and should be considered my preferences and opinions based on my specific experiences.

## 1. System Baseline

1. Log in to the desktop with the credentials `kali:kali`
2. Prepare to patch the deployment by opening up a terminal session
    * `sudo apt update -y && sudo apt upgrade -y`
    * Wait FOREVER.

## 2. Configure Accounts

1. Enable desktop login for `root` account.  I mostly work from the unprivileged account but I like to have options.
    * `sudo apt -y install kali-root-login`
2. Set the `root` password
    * `sudo passwd root`
    * *Enter a pass phrase*
3. While you're at it, change the password for `kali`
    * `passwd`
    * *Enter a DIFFERENT pass phrase*

### 2.1 Optional Configuration for kali Account

Change the username from `kali` to something custom.  In my case, I like to go by "tac0shell" on the command line.  This can be an annoying step so I only recommend doing it if you can afford to be particularly vain (i.e. not on your employer's dime).  As Kali/Linux/desktop environments evolve, more steps than below may be needed to swat bugs to make the whole thing work properly.

1. Logout of your `kali` desktop session and login as `root`
2. You'll probably still have processes running as the `kali` user so in order to continue renaming that account, you'll have to murder those proceesses.  Continuing along this section without doing so will result in the error `user kali is currently used by process ###`

    ```bash
    pkill -9 -u kali
    ```

3. Rename the account, rename the associated group, create new home directory and move all content from old home into new home

    ```bash
    usermod -l tac0shell kali
    groupmod -n tac0shell kali
    usermod -d /home/tac0shell -m tac0shell
    ```

4. Verify the name and owners of the new home directory and its contents are correct, and that the the uid and gid match (usually 1000)

    ```bash
    ls -al /home && ls -al /home/tac0shell && id tac0shell
    ```

5. One other thing that needs to be done is to delete the xfce cache from your previous login session.  Without doing this, you'll log out of `root` and back in to the new `kali`/`tac0shell` account and be presented with no usable desktop environment.

    ```bash
    rm -rf /home/tac0shell/.cache/sessions/*
    ```

6. Log out and log back in as your old, new user.

## 3. Install Software

This section covers my favorite tools and services to use and is in no way comprehensive or even the best software out there.  If you have a strong opinion about a good tool or a better alternative to something listed here, comment below and tell me about it.

### 3.1 Kali Metapackages

Metapackages are pre-packaged software installations of common tools for Kali that are intended to help you rapidly deploy a specific niche instance.  For example, if you are going on a wifi assessment, you can install all the wifi penetration testing tools Kali has to offer by installing the `kali-tools-802-11` metapackage.  If you are low on storage space, you can install `kali-tools-top10`.

1. I like make my hard drives sweat by installing all the things.  At the time of publishing, this installs nearly 9GB of software, but I don't like installing tools mid-engagement.  Based on your circumstances, [make your own metapackage choice](https://www.kali.org/docs/general-use/metapackages/).

    ```bash
    sudo apt install -y kali-linux-everything
    ```

### 3.2 Installing Impacket

Impacket was one of those projects that sometimes came installed with Kali metapackages, but their use was complex and often inconsistent with what random tutorials online told you to do.  As I've found with using Docker in certain projects, it's best not to mess around with the random scripts you find on the system with "impacket" in the name and just go straight to the github source and install it according to the maintainers (originally SecureAuth, now [Fortra](https://github.com/fortra/impacket))

```bash
# First, we have to install pipx
sudo apt update -y
sudo apt install pipx -y
# ...snip...
pipx ensurepath
python -m pipx install impacket
```

Once that is complete, you'll have to either log out of your shell session and reopen it to make sure the results from `pipx ensurepath` take effect, or you can just run `source ~/.zshrc` for impacket's path to be included in your `$PATH`.

### 3.3 Terminal Software

I like to use `terminator` to interact with the command line.  Additionally, I will add a "professional" profile that I can quickly change to when I need to collect screenshots for a professional report.  There are various reasons for this but people print things more than we thought they would 20 years ago, especially in government, and printing a black terminal looks awful and can chew up a lot of ink.  You also don't want to screen capture terminals with any sort of transparency as, in the best case scenario, it can be distracting look at and, at worst, your best screenshot may inadvertently have sensitive data displaying behind it and you don't notice until the engagement window has passed.

1. `sudo apt install terminator`
2. Open a Terminator session.  Right click in the main window and select Preferences.
3. In the Global tab, select the checkbox for "Re-use profiles for new terminals."
4. Click on the Profiles tab and create a new profile, *Professional*.  This will be a profile with settings friendly to report-writing.
5. With your new profile highlighted, click on the Scrolling tab and check the "Infinite Scrollback" setting.
6. In the "Colors" tab, make sure the selected "Built-in scheme" is "Black on white" or whatever your preference may be.  At the time of writing, this was not a great selection for my chose zsh prompt colors.  I will have to come back to this.

If you prefer to use the default Kali Terminal Emulator, here are the same steps to professionalize the terminal screen for reports.

1. File > Preferences > Appearance
    1. Color scheme: BlackOnWhite or BreezeModified
    2. Application transparancy: 0%

2. File > Preferences > Behavior
    1. Unlimited history: Selected
    2. Confirm multiline paste: Checked
    3. Ask for confirmation when closing: Checked
    4. Open new terminals in current working directory: Checked

### 3.4 Screenshot Software

I love the Flameshot software for my screenshots.  It has markup tools available during the capture and gives me the option to save it, copy it to my clipboard, immediately upload it to imgur.com, pixelate information, resize my capture area, and all sorts of useful stuff that I haven't found anywhere else for free.

From the command line:

1. Run `sudo apt install -y flameshot`
2. Go to the Kali xfce menu in the top-left and click on Settings > Keyboard > Application Shortcuts > +Add
3. In the "Command" field, type `/usr/bin/flameshot gui` and click "Ok"
4. Press your desired shortcut/hotkey

### 3.5 ServerSetup by Noah Powers

[ServerSetup](https://github.com/noahpowers/ServerSetup) is a recent project suggested to me with which I don't have extensive experience.  It appears to be a shortcut to deploying common server services often needed during an engagement (Mail Server, SMB Share, HTTPS C2).  As such, I've made a note of it here but may either remove or expand on it in the future, depending on whether I find it useful for my style of pentesting.  Follow the install instructions on the GitHub page for the most up-to-date process.

## 4. Quality of Life Configurations

### 4.1 Customize zsh Experience

My preferred shell prompt settings for `.zshrc` can be found at <https://gitlab.com/patrickriggs/new-kali-vm/-/blob/main/zshrc>.  I enjoy a prompt with a ticking timestamp that will put a marker on my pen testing screenshots.  "When did you do That Thing?"  "It's on the screenshot."  

In addition, there are some shell history tweaks that I prefer, including date and timestamping entries, and immediately writing to the history file instead of waiting until I cleanly exit my shell session.  One thing I also do that does bring it's own challenges is I thread all my terminal session histories into the same single-stream which, at times, make it harder to determine the context of a given command.  

### 4.2 Power and Screen Lock Settings

1. Left-click on the battery icon on the top-right of the xfce desktop
2. In the Power Manager window, click on the "Display" tab and toggle the master switch on the top-right of that window so it appears to be off
3. Move the "Black after" slider all the way to the left to set it as "Never"
4. In the "Security" tab, change "Automatically lock the session" to "Never"

Alternatively, you can temporarily prevent the whole thing from locking by ignoring these settings, which are persistent, and toggling the "Presentation mode" slider that appears when you click on the battery icon.

### 4.3 Timezone Settings

1. Right-click the time on the task bar, click Properties
2. Set your timezone and display preferences
3. Or you can set the timezone on command line

    ```bash
    timedatectl list-timezones | grep *Berlin* or *New York*
    sudo timedatectl set-timezone *chosen timezone*
    ```

## 5. Article Changelog

2024-09-05:

* Wrote a newer, more concise introduction
* Added a section for [installation of Impacket](#32-installing-impacket)
* Minor addition to the [Terminal Software](#33-terminal-software) section regarding terminal transparency
* Refactored markdown for multi-line terminal instructions
* Restructured the order of some sections to alighn with what I consider more important than others
