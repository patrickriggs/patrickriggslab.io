---
id: 576
title: 'BloodHound: Incompatible Collector'
date: '2022-09-11T15:16:05+02:00'
author: Patrick
layout: single
guid: 'https://patrickriggs.com/blog/?p=576'
permalink: /bloodhound-incompatible-collector/
categories:
    - Hacking
    - IT
tags:
    - hacking
    - sharphound
    - troubleshooting
excerpt: If you’re having problems with your SharpHound output uploading to BloodHound, you may be victim of an old version/new version incompatibility.
header:
    teaser: /assets/images/bloodhound.png
---
![]({{ page.header.teaser }}){: .feature-image .align-left }
If you’re having problems with your SharpHound output uploading to BloodHound, you may be victim of an old version/new version incompatibility.

“File created from incompatible collector” and “NaN%” are super frustrating, particularly because you’ll only come across this roadblock when you’re in the throes of an engagement. In my case, I encountered this in both my 3rd OSCP attempt and in the TryHackMe room, “[Post-Exploitation Basics](https://tryhackme.com/room/postexploit).”

<figure class="wp-block-image size-full" markdown="1">
!["Incompatible collector" results after trying to click-and-drag upload my SharpHound.ps1 loot.zip file](/assets/images//2022/09/incompatible-collector.png)
<figcaption>“Incompatible collector” results after trying to click-and-drag upload my SharpHound.ps1 loot.zip file</figcaption></figure>## Solution

Use `SharpHound.exe` to collect your domain information, not `SharpHound.ps1`.

<figure class="wp-block-image size-full" markdown="1">
![](/assets/images//2022/09/locate-SharpHound-exe.png)
<figcaption>Location of SharpHound.exe tool on Kali</figcaption>
</figure>
<figure class="wp-block-image size-full" markdown="1">
![Transferring BloodHound.exe with $ python3 -m http.server 81](/assets/images//2022/09/download-sharphound-exe.png)
<figcaption>Transferring `SharpHound.exe` with `$ python3 -m http.server 81`</figcaption></figure>
<figure class="wp-block-image size-full" markdown="1">
![Successfully using SharpHound.exe in PowerShell.](/assets/images//2022/09/run-sharphound-exe.png)
<figcaption>Using SharpHound.exe</figcaption></figure>
<figure class="wp-block-image size-large" markdown="1">
![Transferring the SharpHound loot with scp](/assets/images//2022/09/download-loot-1024x349.png)
<figcaption>Transferring the SharpHound loot with `scp`</figcaption></figure><figure class="wp-block-image size-full" markdown="1">
![Successful upload to BloodHound](/assets/images//2022/09/click-and-drag-success.png)
<figcaption>Successful upload to BloodHound</figcaption></figure>

ACHTUNG!: I don’t think this will solve every instance of “incompatible collector,” but it worked for me today in my TryHackMe use-case. If this doesn’t work for you, look into the following options:

1. Removing the Kali repo install of BloodHound and [cloning from the project](https://github.com/BloodHoundAD/BloodHound)
2. Downgrading your install of BloodHound back to when `SharpHound.ps1` worked — try 3.0
3. The use of `bloodhound.py` instead of SharpHound to collect domain information

## Explanation

In the case of the TryHackMe room’s AttackBox, which is usually the route I go when I’m having trouble with my own Kali VM, the problem was that the SharpHound deployment on the environment’s Windows Server was only equipped with `SharpHound.ps1` while the Ubuntu AttackBox had BloodHound 4.1.0 and my Kali VM used 4.2.0.

According to one of the project maintainers, rvazarkar, the “newest versions” of BloodHound [do not make use of SharpHound.ps1](https://github.com/BloodHoundAD/BloodHound/issues/516#issuecomment-1100291883). At the time of writing, the latest version BloodHound is 4.2.0, released this past August. The comment by rvazarkar was made on April 15th when 4.1.0 was the current release, making it the newest version at the time.

<figure class="wp-block-image size-full" markdown="1">
![TryHackMe, Post-Exploitation Basics AttackBox BloodHound version 4.1.0](/assets/images//2022/09/AttackBox-version.png)
<figcaption>TryHackMe, Post-Exploitation Basics AttackBox BloodHound version 4.1.0</figcaption></figure>
<figure class="wp-block-image size-full" markdown="1">
![BloodHound maintainer indicating there was a sunset for SharpHound.ps1](/assets/images//2022/09/no-SharpHound-ps1.png)
<figcaption>BloodHound maintainer indicating there was a sunset for `SharpHound.ps1`</figcaption></figure>

## Sources

There was no one-line answer available on the first page of Google that I could see, hence the reason for this post. However, reading this [GitHub issue](https://github.com/BloodHoundAD/BloodHound/issues/516) on the project page led me to believe there was probably a version mismatch between the SharpHound pre-staged on the Windows Server and the BloodHound installed on the AttackBox and my Kali VM.