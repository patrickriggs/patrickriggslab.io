---
id: 351
title: '&#8220;Cards Against Humanity&#8221; Remotely'
date: '2020-04-03T22:36:01+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=351'
permalink: /cards-against-humanity-remotely/
header:
    teaser: /assets/images/2020/04/cards_against_humanity.jpg
excerpt: "I am typing this for my friends who have responded to my call on social media for a good old fashioned game of (socially distant) Cards Against Humanity. What follows are the requirements to get everyone up on a knock-off game"
categories:
    - General
tags:
    - 'Cards Against Humanity'
    - COVID-19
    - Games
    - 'Social Distance'
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

## Introduction

I am typing this for my friends who have responded to my call on social media for a good old fashioned game of (socially distant) Cards Against Humanity. What follows are the requirements to get everyone up on a knock-off game.

Requirements:

- Proficiency with Zoom (only the host needs a paid account)
    - Web-based: <https://zoom.us/join>
    - Download the software: <https://zoom.us/download>
- Experience playing Cards Against Humanity
    - You do NOT need a physical set of cards

Though there were two obvious services that offer a Cards Against Humanity-style game, at the time of this writing I have only successfully used <http://playingcards.io>.

## playingcards.io

This is the closest experience to a tabletop game as I’ve seen. There is no auto-dealing, no mechanism to pass dealer responsibilities around the table, and nothing preventing other players from accidentally clicking on each others’ cards to flip them over. It is harder to use with a laptop touchpad than a regular mouse. It also limits participants to 6 and there is no scoring mechanism, so use a notepad.

I have successfully run two Zoom/Playingcards.io sessions. It work best when someone familiar with the way the platform is organized is permanent dealer and simply directs who is next Card Czar.

<figure class="wp-block-image size-large" markdown="1">
[![](https://patrickriggs.com/blog/wp-content/uploads/2020/04/remote_insensitivity01-3-1024x618.png)](https://patrickriggs.com/blog/wp-content/uploads/2020/04/remote_insensitivity01-3.png)
</figure>

I recommend starting with the board settings on the top-left of the screen and changing the names on top of each player position (#2). That will remove ambiguity of whose cards belong to who as you start to deal.

### Gameplay

Whether you choose to have a permanent dealer or assign dealing duties to the rotating Card Czar, the dealer begins by dealing from the “white”/pink card deck (#1) to all the positions along the left and right (#2). Each player drags their cards, one by one, to their “private zone” (#3). In this zone, only each respective player can see their flipped cards. It will not show to the other players.

The Card Czar draws from the left “black”/dark purple deck (#4) and reads the card aloud into the Zoom meeting. Each player makes their selection from the cards in their hand (#3) and drags it into the middle of the table (#5). The Card Czar reads the card aloud, flips each cards in the middle, then announces the winning card.

The dark purple card and pink cards are placed in their respective discard piles (#6) and the Card Czar responsibilities rotate. The dealer places replacement cards for each player in their respective piles.

<div class="wp-block-image"><figure class="alignright size-large" markdown="1">
[![Order of card placement for responses with multiple answers.](https://patrickriggs.com/blog/wp-content/uploads/2020/04/2cards.png)](https://patrickriggs.com/blog/wp-content/uploads/2020/04/2cards.png)
</figure></div>

The only difficulty with this game lies with cards that have multiple responses. You are going to want to reverse the natural order of the cards to ensure your answers are read in the proper manner.

## Other Platforms

The other Cards Against Humanity platform seems more complex, is highly configurable, and has numerous expansion packs. However, I was unable to test it out without other players staged to immediately enter the room. Some day, I might grab a couple of people to help test out <https://www.pretendyoure.xyz/zy/>. Who can say no to a website that names their servers “dickfingers” and “A windmill full of corpses.”