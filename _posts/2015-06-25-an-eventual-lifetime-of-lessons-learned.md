---
id: 6
title: 'An Eventual Lifetime of Lessons Learned'
date: '2015-06-25T06:48:13+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=6'
permalink: /an-eventual-lifetime-of-lessons-learned/
categories:
    - General
toc: false
---

<div class="entry-content">35 isn’t too late for two things:

1. Collecting your thoughts into a journal-like form (it’s NOT a diary!)
2. Have your first kid

Topics you will find discussed on this blog include information technology (web development, Linux, information security), learning to become a father (I have no idea what’s going on), culture and subculture (American/European, punk, retro-Internet), and whatever else floats from my grey matter to my fingertips.

</div>