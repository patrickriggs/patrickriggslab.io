---
id: 171
title: 'The 44th Annual Cambridge Beer Festival, Day 3 &#038; 4'
date: '2017-05-24T21:45:55+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=171'
permalink: /cambridge-beer-festival-day-3-4/
header:
    teaser: /assets/images/2017/05/wp-1495837873426.-972x972.jpg
categories:
    - 'People &amp; Culture'
tags:
    - beer
    - Cambridge
    - CAMRA
    - festival
    - 'real ale'
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

Due to previous obligation, I missed day 2, so we’ll skip straight to Wednesday and Thursday, days 3 and 4 of the 44th Annual Cambridge Beer Festival.

<span class="feature-image" markdown=1>
![](/assets/images/2017/05/pixlr-300x300.jpg "Three panel picture of a Scotch egg in different stages of being consumed")
</span>

<figcaption>After a few beers, one gets a little peckish, so I had my first Scotch egg</figcaption>

For Wednesday, my party consisted of my wife, son, and father and mother in-law. We made it to the festival shortly after its evening opening time at 5pm. Once we sailed beyond the unwashed masses with no CAMRA membership and we slipped on through the member’s entrance, I made a bee-line straight for the “H” section of the alphabetized brewers on the hunt for Harvey’s “[Prince of Denmark](https://untappd.com/b/harvey-and-son-harveys-prince-of-denmark/130072),” last year’s overall winner of the festival. The brew masters had chosen not to open the cask on Monday so my gratification had been delayed until then, and it was worth it. On Thursday, I again sought familiar comfort and had a second serving of [Gigesa Guji](https://untappd.com/b/three-blind-mice-brewery-gigesa-guji/2021339) by Three Blind Mice, the first being from my [first visit on Monday](https://patrickriggs.com/blog/44th-annnual-cambridge-beer-festival-day-1/).

Side note: it has taken me 3 years in this country to finally have my first Scotch egg. It was delicious!

## Cambridge Beer Festival: Day 3

### Prince of Denmark by Harvey and Son

2016’s joint winner, alongside [Cambridge Brewing Company’s](http://www.cambridgebrewingcompany.com/) [Chicken Porter](https://untappd.com/b/the-cambridge-brewing-company-chicken-porter/1095841), [Harvey and Son’s](https://www.harveys.org.uk/) Prince of Denmark was in my personal top 3 from that year. The batch offered now in 2017 is just as phenomenal as the last. It’s a rich, thick molasses flavor that goes down way to smoothly to not have another.

### Flat white Porter by Adnams

Coffee stout with a vanilla nose. Rather mild flavor for it’s type. 5% ABV.

### Bitter Invention of Satan by Bexar

Not that bitter. Served out of a break room coffee dispenser-like device with [KeyKegs](http://www.sacrebrew.com/2015/04/keykegs/) inside. Cold. Tastes sediment-y, which I like. A little sour and dry. 8.6 ABV will clobber you if you aren’t careful.

### Papa Steve by Bexar

9.0. Dark, strong, kind of creamy, tangy rich, and medium heavy thick.

## Cambridge Beer Festival: Day 4

### Half Bretted Cherry Flicker by Three Blind Mice Brewery

I had trouble tasting the chocolate promised in the description of this one. I found it to be a very sour cherry flavor. This one wasn’t really my cup of tea.

### Russian Blud by Three Blind Mice Brewery

The Untappd flavor profile described this one as “sweet,” which I could not detect. This Russian WMD tasted strong but not the whopping 11% ABV sucker punch to the jaw I expected. I can see this one sneaking up on you like a Victorian thug with a cudgel.

### Gigesa Guji by Three Blind Mice Brewery

Like the Prince of Denmark I had on day 3, I sought familiar refuge from the unknown by going back to my favorite serving from day 1. I had spent most of day 4 in the same corner of the big festival tent. That locale has been a very popular location for all three evenings so far. Three Blind Mice brewery has some very interesting and unique offerings. I would be surprised if the don’t walk away with some sort of award this year.

On day 1, I had a small sample of [Lady Grey IPA](https://untappd.com/b/three-blind-mice-brewery-lady-grey-ipa/2107591) that I did not review which had sold out by Wednesday. I thought it was very good and evidently so did many others.

### Cherry Prasto’s Porter by Boudicca

This one smelled musky, almost chewy or sediment-y, if that makes sense. I liked the nose, but the drink tasted a bit too tart for my personal preferences. I prefer sweeter cherries, and these were pretty sour. It was after this serving I came to the realization that I was unconsciously on the hunt for a Belgian Kriek beer (whose name escapes me) I had discovered in Bruges a couple years back. I have now put cherry beers on the same “avoid” list as heavy hops, for the time being.

### Aecht Schlenkerla Rauchbier – Märzen From Schlenkerla

To end my night, I decided to venture into mainland Europe and have something from the foreign beer stand. I asked and paid for a third, and received half a pint of a liquid campfire. God bless the Germans.