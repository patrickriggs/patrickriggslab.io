---
id: 38
title: 'Review: Certified Ethical Hacker Exam Prep Course'
date: '2016-08-16T21:12:01+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=38'
permalink: /review-certified-ethical-hacker-exam-prep-course/
header:
    teaser: /assets/images/2016/08/cehv9-banner-1-972x398.png
categories:
    - IT
tags:
    - CEH
    - certification
    - 'certified ethical hacker'
    - education
    - hacking
    - 'professional development'
    - security
toc: false
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

Last week I had the fortune to have been paid to attend a remote learning session of [Learning Tree International](https://www.learningtree.com/)‘s [Certified Ethical Hacker Certification Exam Preparation](https://www.learningtree.com/courses/2031/certified-ethical-hacker-ceh-certification-exam-preparation-and-training/) course. Remote means I was connected to the classroom via Adobe Connect. This is the third such time I’ve been able to sit at home, at my own desk and beam in training from across the ocean.

The bottom line on this course is that while I do feel significantly more prepared to take the CEH exam, which is the mission of the course — it’s in the title, I believe the class was tragically lacking the hands-on exercise time I’ve become accustomed to with my professional development training.

In 40 hours of class time, I don’t think it’s an exaggeration to say that we didn’t get more than a couple hours of practical time doing the lab work (some of which was combined lab **and** break time) and I don’t feel confident that, with this course alone, I’m able to retain the dry facts presented in such a quantity that I would pass the exam by much. I might be underselling myself, but I’d rather go in more confident that I currently feel.

This was not the fault of the instructor so much as it was the design of the course. It’s a lot of material to shot gun through and the class is advertised as an Exam Prep course, not a how-to-hack boot camp. To comfortably go through everything, you would have needed at least 8 days, if not the full 2 weeks to give both the lecture and the labs their due attention.

The problem being, and I understand Learning Tree’s decision, is that far fewer companies would release their people for that long for training than currently do for 5 days. I know I certainly wouldn’t have been able to take it. The cost would have been twice what it was.

A third of the material was either identical or familiar from my Security+ certification. A third was vague memories of bashing away on computers in the 90s and other random stuff I picked up over the years, and the last third of it was brand new to me — such as mobile phone hacking. The whole body of knowledge was also wrapped in a delicious tortilla of common sense.

Mmm. Tortillas.

Nonetheless, I’ve scheduled my CEH exam for two weeks from now. I will post back here with the results and compare them with how I feel the class was able to realistically prepare me.