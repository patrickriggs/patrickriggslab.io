---
id: 29
title: 'From the E-Mail Vault: Traveling to Iceland'
date: '2016-01-04T10:54:12+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=29'
permalink: /traveling-iceland/
header:
    teaser: /assets/images/2016/01/iceland-1.jpg
categories:
    - Travel
tags:
    - 'e-mail vault'
    - holiday
    - Iceland
    - vacation
toc: false
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

I’ve had the same primary e-mail address for the last 12 years. I also delete almost nothing and frequently search through my inbox for old e-mails or topics on which I know I’ve written. Sometimes, I even overtly inject my e-mails with search terms and keywords to help future me find it. As such, a friend broadcast on Facebook the need for recommendation for a coming trip to Iceland, that sent me digging for an e-mail to forward for the third time. My wife and I were lucky enough to travel to Iceland for our 2014 anniversary and I had written an e-mail not long after for a friend who was planning a trip there.

## From the E-Mail Vault: Iceland

<blockquote markdown=1>
I think you’ll find this to be one of the best trips you will ever take. Landing in Iceland is like suddenly finding yourself on another planet.

You’ll be flying into Keflavik Airport, which is a 30 minute bus ride from Reykjavik. Once you get to the Reykjavik bus terminal, your best bet if you don’t have a rental is just to grab a taxi from out front. Most Icelanders speak English, but have a print-out of the hotel or destination just in case.

Downtown Reykjavik is small and can be experienced in 2 days, if not quicker. The best parts of <span class="il">Iceland</span> are out in the countryside and generally involve hiking, horseback riding, or some other outdoor activity. We used **<http://eldhestar.is/>**, which was o.k. — not stellar. We went for a morning horseback ride on the famed Icelandic horses and then spent the an afternoon to hike to some “hot springs” with a novice tour guide who was waaaaay faster than me. I was perpetually lagging back with my short Mexican legs, which I’m sure was irritating to the rest of the group. The hot springs turned out to be just a small little pregnant bend in a creek. Not the bubbling, gurgling pond I somehow imagined in my head. Still, it was pretty awesome but I felt we could have done with a better service.

If you do choose to do outdoor stuff, you’re going to need appropriate footwear. Don’t cheat on this. waterPROOF, not just water resistant. Your socks will thank you.

Check out at least one of the several museums. We went to “**Reykjavik 871 +/- 2 Settlement Museum**” which was on top of an old Viking longhouse they recently discovered and gave a pretty good history of how <span class="il">Iceland</span> came to be settled. The remnants of the longhouse was the centerpiece of the underground exhibit. I’m sure any of the other museums would have been equally as awesome, this is just the one we went to.

Here are the notable places at which we ate:

**Glo** – [http://glo.is](http://glo.is/)
- Downtown. Vegetarian/vegan/raw restaurant with a couple of Chicken dishes for the carnivore friends

**Grillmarkadurinn** – [http://grillmarkadurinn.is](http://grillmarkadurinn.is/)
- Downtown. Fantastic, high-class grill house. If you’re going to get adventurous with weird, <span class="il">Icelandic</span> seafood dishes, you can trust this place. Ensure you make reservations.

**Micro Bar** – Austurstræti 6, Reykjavík, <span class="il">Iceland</span>
- Downtown. Not recommending this place for the food, but for the beer. A good one-stop-shop to quickly sample <span class="il">Iceland</span>‘s beer industry.

**Bæjarins Beztu** – Tryggvatagata 1, 101 Reykjavik, <span class="il">Iceland</span>
- North side of Downtown. Famous hot dog stand/food truck featured on *No Reservations* with Anthony Bourdain. DO NOT SKIP THIS. Have one of their hot dogs with as many of the toppings as you can stomach. At least the brown sauce.

The only thing we wish we had done was spend more outdoor time on the island. We were there for a long weekend — about 4 days, and we felt we had gotten most of Reykjavik done in 2. Then we spent 1 doing outdoor stuff. Next trip we’re only using Reykjavik as a transportation hub to head out to the rest of the country for outdoorsy activities. Definitely pay the money to do at least 1 tour while you’re there. Get into their wilderness, if only for an afternoon.

OH! Also, do the **Blue Lagoon**. Yeah, it’s a tourist destination but it’s obligatory and still totally worth it.

</blockquote>

As an addendum, I would rather have made the mistake of doing mostly outdoors-y island stuff and missed out on Reykjavik, rather than doing what we actually did, spending most of our time in Reykjavik and missing out on the nature activities. Oh well. Next time.