---
id: 233
title: 'Intrusion Detection For Windows'
date: '2017-11-07T21:32:51+01:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=233'
permalink: /intrusion-detection-windows/
categories:
    - IT
tags:
    - hacking
    - 'intrusion detection'
    - security
    - Windows
toc: false
---

This morning, I was discussing with a colleague the last time I really felt like I had a handle on Windows under the hood: Windows NT. As soon as I had separated from the Army in 2001, I left Windows system administration behind and wrapped myself in the Linux Blanket of Truth ™.

I’ve had a Windows machine in my house for the vast majority of the last 16 years, but solely as an everyday user of those machines. I never used them as network servers or anything more powerful than a gaming platform. Now that I have a reason to look under the Windows hood (penetration testing), the level of knowledge rot from which I realize I am suffering is quite staggering.

I came across this cheat sheet on reddit during my usual morning perusal and I thought it did a pretty good job at helping me shine my light in the corners of the Windows engine with which I need to be familiar. It’s also nice coming from a sort of incident handler’s perspective, rather than an offensive mindset, for a change.

https://techincidents.com/penetration-testing-cheat-sheet/#