---
id: 145
title: 'Cert Up: Agile Certified Practitioner'
date: '2017-05-07T17:11:35+02:00'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?p=145'
permalink: /pmi-agile-certified-practitioner/
header:
    teaser: /assets/images/2017/05/pmi-acp-972x205.png
categories:
    - IT
tags:
    - agile
    - certification
    - certup
    - goals
    - 'professional development'
    - 'self improvement'
---
<span class="feature-image" markdown=1>
![]({{ page.header.teaser }})
</span>

## Intro

This week, I took and passed the Agile Certified Practitioner’s examination at a Prometrics facility in London.

I had attended a pretty good bootcamp from [Firebrand training](http://www.firebrandtraining.co.uk/) back in November (it’s May) but due to the onerous requirements levied by PMI (an application to even take the exam followed by a “random” audit of said application) a substantial amount of knowledge rot occurred.

Luckily, I had implemented an Agile methodology (Scrum) on my main project a year ago, so the pace of memory atrophy was slowed.

My bootcamp instructor (hat-tip to [Cameron Stewart](https://www.linkedin.com/in/cstewart/)) heartily recommended an exam prep book, *PMI-ACP Exam Prep* by Mike Griffiths. I couldn’t find it at a reasonable price on Amazon.co.uk, but did on Amazon.com. Still, I didn’t bother ordering it for another couple months, thinking it would only be a few weeks until exam time.

## Preparation

My modus operandi for test preparations is to casually study, increasing in intensity as the exam approaches. As such, I was still studying the morning of the exam, sitting at a Leon’s at King’s Cross eating my second breakfast.

[![A photo of the recommended Agile exam study guide, PMI-ACP Exam Prep by Mike Griffiths.](/assets/images/2017/05/pmi-agile_certified_practitioner_book-1024x682.jpg)](/assets/images/2017/05/pmi-agile_certified_practitioner_book.jpg)The recommended Agile exam study guide, PMI-ACP Exam Prep, and my stopping point before sitting for the exam

I made it into chapter 4, team performance, before realizing I had better get to the testing center. Despite this, I was not overly concerned. Chapter 1 reminded me the exam was more about having an agile mindset than it was memorizing rote facts, like port numbers. I had been running a Scrum team for over a year at this point and, no matter how imperfect the implementation was or how many daily scrums I let slip by, the intent and lessons were there.

The language from the bootcamp was an arm’s length away and coming back to me, evoked by *Exam Prep*. The book was helping, even if I skimmed over much of it or didn’t finish the last half. The author was fairly clear on whether a topic was likely to be on the exam, or if it was merely background context. Skip-able at your own discretion.

## Into the Breach

120 questions in 3 hours — I was gunning the questions down at a steady, plodding pace. I finished with about 25 minutes to spare to go back over the 20 questions I marked for review. I changed 4 of those answers, let the remainder stand, and walked out with my PMI-ACP certification.

Relief!

The most fidelity you get with your marks are an overall pass/fail, and one of three results for each of the 7 domains: *proficient*, *moderately proficient*, and *below proficient*. I scored 5 moderately proficients and 2 proficients. One proficient was in a domain to which I had not reached the chapter.

Overall, I thought the exam was challenging but fair. I liked Firebrand’s bootcamp, and I liked *PMI-ACP Exam Prep* by Mike Griffiths. Agile is a solid anchor I can drop when chaos reigns around me.