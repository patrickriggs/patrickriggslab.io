---
id: 499
title: 'VMware Workstation: "MonitorLoop" power on failed'
date: '2021-04-20T19:49:32+02:00'
author: Patrick
layout: single
guid: 'https://patrickriggs.com/blog/?p=499'
permalink: /vmware-workstation-monitorloop-power-on-failed/
categories:
    - IT
tags:
    - troubleshooting
    - VMWare
toc: false
---

Are you trying to power on a virtual machine on your ESXi host and VMware Workstation is telling you, “Unable to change virtual machine power state: Module ‘MonitorLoop’ power on failed.”

There are two reasons you’d be seeing this.

1. Your datastore is running out of disk space
2. You recently experienced a power outage and forgot to turn your NAS datastore back on. Turn it on.

<figure class="wp-block-image size-large is-style-default" markdown="1">
![](/assets/images/2021/04/image-1024x702.png)
<figcaption>How esoteric</figcaption></figure>Hat-tip to Duncan Epping at [yellow-bricks.com](http://www.yellow-bricks.com/2018/06/12/module-monitorloop-power-on-error-failed-when-powering-on-vm-on-vsphere/).