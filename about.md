---
layout: single
title: About
date: '2017-04-26T21:47:56+02:00'
permalink: /about/
author: Patrick
---

[![Head shot from 2018](/assets/images/2022/07/2018-Patrick_Headshot-small-200x300.jpg)](/assets/images/2022/07/2018-Patrick_Headshot-small.jpg){: .align-right}

## $ cat /etc/motd

5 time OSCP exam participant!

## $ whoami

Penetration testing, coffee-drinking, counter-culture enthusiast. I love computers, soccer, obsessive progression, and progressive obsessions. I aspire to have my very own CVE some day and, when the kids are old enough to mind themselves, will moonlight once again on stage as a rock star 🎸.

## $ cat ~/proof.txt

<div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.credly.com" data-share-badge-id="ebf6a2a5-c14a-48e5-ac96-a506b9070cfb"> </div><script async="" src="//cdn.credly.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.youracclaim.com" data-share-badge-id="3778f4bc-73d0-4b61-a96d-3137a3581500"> </div><script async="" src="//cdn.youracclaim.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.credly.com" data-share-badge-id="b24f0a06-9da2-46eb-a5d0-a05db091b20e"> </div><script async="" src="//cdn.credly.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.youracclaim.com" data-share-badge-id="502f213d-fa21-4407-a52f-0a4baf8f130b"> </div><script async="" src="//cdn.youracclaim.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.youracclaim.com" data-share-badge-id="2b86b45f-5361-4437-8ec4-72edee4083bd"> </div><script async="" src="//cdn.youracclaim.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.credly.com" data-share-badge-id="a9a4fb22-c9ac-47c3-ac3d-82d817ef8bd0"> </div><script async="" src="//cdn.credly.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.credly.com" data-share-badge-id="24a23cb1-14d0-4ed4-99b2-9fff22a242b7"> </div><script async="" src="//cdn.credly.com/assets/utilities/embed.js" type="text/javascript"></script><div data-iframe-height="270" data-iframe-width="150" data-share-badge-host="https://www.credly.com" data-share-badge-id="9dd15946-2e02-4e6e-9a3d-12fb37b19662"> </div><script async="" src="//cdn.credly.com/assets/utilities/embed.js" type="text/javascript"></script>

- GIAC Penetration Tester (GPEN)
- GIAC Web Application Penetration Tester (GWAPT)
- GIAC Certified Incident Handler (GCIH)
- GIAC Python Coder (GPYC)
- GIAC Advisory Board Member
- Certified Scrum Master (PSM I)
- Security\+
- Certified Ethical Hacker (C\|EH)
- Expired PMI-ACP
