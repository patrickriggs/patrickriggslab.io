---
id: 297
title: 'My Bookmarks'
author: Patrick
layout: single
guid: 'http://patrickriggs.com/blog/?page_id=297'
tags:
  - links
---

Personal bookmarks and resources I want at my fingertips anywhere I go

## INFOSEC, Hacking, Penetration Testing

- [Hacksplaining](https://www.hacksplaining.com/) - Concise explanations of common security problems for software developers.
- [lolbas](https://lolbas-project.github.io/) – Living off the Land Binaries and Scripts. Native tools included in Windows that can be used in a penetration test.
- [gtfobins](https://gtfobins.github.io/) – Curated list of Unix binaries that can be exploited by an attacker to bypass local security restrictions. “Living off the land.”
- [Openwall Wordlists](http://www.openwall.com/wordlists/) – Wordlists for password cracking.
- [Aperi'Solve](https://www.aperisolve.com/) - Online platform that performs layer analysis on images.
- [CI/CD Goat Project](https://github.com/cider-security-research/cicd-goat) - A deliberately vulnerable continuous integration, continuous delivery environment for hacking practice.  Similar to OWASP Juice Shop, only for CI/CD pipelines.
- [NTLM Relaying 101](https://jfmaes-1.gitbook.io/ntlm-relaying-like-a-boss-get-da-before-lunch/) - An NTLM relaying workshop and lab.
- [Subdomain Enumeration via Archived TLS Certs](https://crt.sh)

## Information Technology - General

- [Understanding SDDL Syntax](https://itconnect.uw.edu/tools-services-support/it-systems-infrastructure/msinf/other-help/understanding-sddl-syntax/) - A great explainer of Windows Security Descriptor Definition Language published by the University of Washington (with permission from the Stanford author).  A must-have if you're examining security attributes of Windows artifacts, like services.
- ["PAT or JK" ASCII Art Generator](http://patorjk.com/software/taag/) – ASCII Art for your /etc/motd
- [Mind Maps by Aman Hardikar](https://amanhardikar.com/mindmaps.html) - Brain compasses for mental navigation of technical tasks
- [cheat sheet filetype:pdf site:sans.org](https://www.google.com/search?biw=1273&bih=598&ei=i59JXbfcK9G2ggeUvquQCw&q=cheat+sheet+filetype%3Apdf+site%3Asans.org&oq=cheat+sheet+filetype%3Apdf+site%3Asans.org&gs_l=psy-ab.3...94589.95339..96062...0.0..0.224.600.5j0j1......0....1..gws-wiz.OUdCLLwI0k8&ved=0ahUKEwj3zea1yu7jAhVRm-AKHRTfCrIQ4dUDCAo&uact=5) – Google search straight to all the SANS Institute PDF cheat sheets

## OSCP-Specific Resources

- [Web Testing on OSCP](https://guif.re/webtesting) - Common web testing tasks in the OSCP environment
- [Pentesting Cheatsheet](https://github.com/Kitsun3Sec/Pentest-Cheat-Sheets) - Steers away from using Metasploit
